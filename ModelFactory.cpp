#include "ModelFactory.h"
#include "Tree.h"


ModelFactory::ModelFactory()
{
	mm = new ModelManager();
}

AbstractModel* ModelFactory::createQuad(Shader* shader)
{
	Quad* qua = new Quad(shader);

	this->mm->addModel(qua);

	return qua;
}

AbstractModel* ModelFactory::createTriangle(Shader* shader)
{
	Triangle* tri = new Triangle(shader);

	this->mm->addModel(tri);
	
	return tri;
}

AbstractModel* ModelFactory::createPlain()
{
	Model* plain = new Model();
	plain->load("Models/teren.obj");

	this->mm->addModel(plain);

	return plain;
}

AbstractModel* ModelFactory::createBuilding()
{
	Model* build = new Model();
	build->load("Models/building.obj");

	this->mm->addModel(build);

	return build;
}

AbstractModel* ModelFactory::createSkydome()
{
	Model* sky = new Model();
	sky->load("Models/skydome.obj");

	this->mm->addModel(sky);

	return sky;
}

AbstractModel* ModelFactory::createZombie()
{
	Model* zom = new Model();
	zom->load("Models/zombie.obj");

	this->mm->addModel(zom);

	return zom;
}

AbstractModel* ModelFactory::createSuziFlat()
{
	SuziFlat* suziFlat = new SuziFlat();

	this->mm->addModel(suziFlat);

	return suziFlat;
}

AbstractModel* ModelFactory::createSuziSmooth()
{
	SuziSmooth* suziSmooth = new SuziSmooth();

	this->mm->addModel(suziSmooth);

	return suziSmooth;
}

AbstractModel* ModelFactory::createTree()
{
	Model* tree = new Model();
	tree->load("Models/tree.obj");

	this->mm->addModel(tree);

	return tree;
}

AbstractModel* ModelFactory::createSphere()
{
	Sphere* shere = new Sphere();

	this->mm->addModel(shere);

	return shere;
}

AbstractModel* ModelFactory::createLight()
{
	Light* light = new Light();

	this->mm->addModel(light);

	return light;
}

AbstractModel* ModelFactory::createWall()
{
	Model* wall = new Model();
	wall->load("Models/zed.obj");

	this->mm->addModel(wall);

	return wall;
}

ModelManager* ModelFactory::getMM()
{
	return this->mm;
}
