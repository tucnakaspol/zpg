#pragma once
#include "AbstractModel.h"
#include "Models/plain.h"
#include "SOIL.h"

class Plain : public AbstractModel
{
public:
	Plain();

	void setModel();
	void drawModel();
};

