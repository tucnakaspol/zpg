#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>  
#include<string>
#include<fstream>
#include<sstream>
#include<iostream>
#include<cerrno>
#include <../glm/vec3.hpp> // glm::vec3
#include <../glm/vec4.hpp> // glm::vec4
#include <../glm/mat4x4.hpp> // glm::mat4
#include <../glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <../glm/gtc/type_ptr.hpp> // glm::value_ptr
#include "Transformation.h"
#include "Camera.h"
#include "ShaderLoader.h"
#include "SOIL.h"
#include "AbstractShader.h"



class Shader : public ShaderLoader, public AbstractShader
{
protected:
	Camera* camera;
public:
	// Reference ID of the Shader Program
	
	// Constructor that build the Shader Program from 2 different shaders
	Shader(Camera* camera, const char* vertexFile, const char* fragmentFile);

	// Activates the Shader Program
	void ActivateA(glm::mat4);
	void ActivateA();
	void Activate(glm::mat4 M);


	void ActivateWithTexture(glm::mat4 M, GLuint textureId, int position);

	// Deletes the Shader Program
	void Delete();

	Camera* getCamera();


	GLuint getShaderId();

};

