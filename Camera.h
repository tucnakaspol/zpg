#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>  

#include <iostream>
#include <../glm/vec3.hpp> // glm::vec3
#include <../glm/vec4.hpp> // glm::vec4
#include <../glm/mat4x4.hpp> // glm::mat4
#include <../glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <../glm/gtc/type_ptr.hpp> // glm::value_ptr
#include <../glm/gtx/rotate_vector.hpp>
#include <../glm/gtx/vector_angle.hpp>
#include <../glm/gtx/matrix_decompose.hpp>

class AbstractShader;

class Camera
{
private:
	glm::vec3 eye;
	glm::vec3 look;
	glm::vec3 up;

	glm::mat4 projection;
	glm::mat4 view;

	int width;
	int height;
	float speed = 0.1f;
	float sensitivity = 100.0f;
	bool firstLeftClick = true;
	bool firstRightClick = true;

	GLuint currConst;
	GLuint currObject;
	GLuint currShader;
	glm::mat4 currMatrix;



	static Camera* instance;
	Camera(int width, int height);
	Camera();

public:
	static Camera* getInstance(int width, int height);
	static Camera* getInstance();
	glm::vec3 getEye();
	glm::mat4 getCamera();
	void Inputs(GLFWwindow* window);
	void callSpace();
	void callW();
	void change(int width, int height);
};

