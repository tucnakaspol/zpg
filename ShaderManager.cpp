#include "ShaderManager.h"
#pragma once

ShaderManager* ShaderManager::instance = 0;

ShaderManager::ShaderManager()
{
}

int ShaderManager::countOfShaders()
{
	return shaders.size();
}

void ShaderManager::addShader(Shader* shader)
{
	shaders.push_back(shader);
}

Shader* ShaderManager::getShader(int index)
{
	return shaders[index];
}

Shader* ShaderManager::getShaderWithId(GLuint id)
{
	for (int i = 0; i < this->countOfShaders(); i++)
	{
		if (this->getShader(i)->getShaderId() == id)
		{
			return this->getShader(i);
		}
	}

	return this->getShader(0);
}

ShaderManager* ShaderManager::getInstance()
{
	if (instance == 0)
	{
		instance = new ShaderManager();
	}

	return instance;
}
