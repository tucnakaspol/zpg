#pragma once
#include "AbstractModel.h"
#include "Shader.h"
#include "Transformation.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "Textures.h"


class DrawableObject
{
protected:
	AbstractShader* shader;
	AbstractModel* model;
	Transformation* transform;
	float angle;
	int positionOfTexture;
	glm::mat4 matrix;

public:
	DrawableObject(AbstractModel* model, AbstractShader* shader, int positionOfTexture);

	void drawWithTexture();

	void drawSkydome();

	void translateObject(float x, float y, float z);

	void translateObjectFromStart(float x, float y, float z);

	void scaleObject(float x, float y, float z);

	void rotateObject(float x);

	void setShader(Shader* shader);

	GLuint getShaderId();

	AbstractModel* getModel();

	AbstractShader* getShader();

	Camera* getCamera();

	int getTexturePos();
	glm::mat4 getMatrix();
	void setMatrix(glm::mat4 matrix);
};
