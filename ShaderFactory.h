#pragma once
#include "ShaderManager.h"
#include "Camera.h"
#include "ConstantShader.h"
#include "MultipleLightShader.h"

class ShaderFactory
{
private:
	ShaderManager* sm;
public:
	ShaderFactory();

	Shader* createConstant(Camera* camera);

	MultipleLightShader* createLambert(Camera* camera);

	Shader* createPhong(Camera* camera);

	Shader* createTextureShader(Camera* camera);

	MultipleLightShader* createMultipleLightShader(Camera* camera);

};

