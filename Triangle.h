#pragma once
#include "AbstractModel.h"
#include "Shader.h"

class Triangle  : public AbstractModel
{
private:
	float points[32] = {
		-1.0f, -0.5f, 0.5f, 1.0f, 0.5f, 0.5f, 0.5f, 1.0f,
		-0.5f, 1.0f, 0.5f, 1.0f, 0.5f, 0.5f, 0.5f, 1.0f,
		1.0f, 0.5f, 0.5f, 1.0f, 0.5f, 0.5f, 0.5f, 1.0f,
	};

	Shader* shader;
	Transformation* transform;
	float angle;

public:
	Triangle(Shader* shader);

	void setModel();
	void drawModel();


};

