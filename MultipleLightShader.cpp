#include "MultipleLightShader.h"

void MultipleLightShader::ActivateWithTexture(glm::mat4 M, GLuint textureId, int position)
{
	glUseProgram(shaderProgramID);

	glm::vec3 lightPositions[4];
	glm::vec4 lightColors[4];


	for (int i = 0; i < LightManager::getInstance()->countOfLight(); i++)
	{
		lightColors[i] = LightManager::getInstance()->getLight(i)->getColor();
		lightPositions[i] = LightManager::getInstance()->getLight(i)->getPosition();
	}



	GLuint idLightColor = glGetUniformLocation(shaderProgramID, "lightColours");
	glUniform4fv(idLightColor, LightManager::getInstance()->countOfLight(), glm::value_ptr(lightColors[0]));

	GLuint idLightPos = glGetUniformLocation(shaderProgramID, "lightPositions");
	glUniform3fv(idLightPos, LightManager::getInstance()->countOfLight(), glm::value_ptr(lightPositions[0]));



	GLint uniformID = glGetUniformLocation(shaderProgramID, "textureUnitID");
	glUniform1i(uniformID, position);

	GLint idlightCount = glGetUniformLocation(shaderProgramID, "numberOfLights");
	glUniform1f(idlightCount, (float)LightManager::getInstance()->countOfLight());

	
	GLint idModelTransform1 = glGetUniformLocation(shaderProgramID, "cameraMatrix");
	glUniformMatrix4fv(idModelTransform1, 1, GL_FALSE, glm::value_ptr(camera->getCamera()));
	GLint idModelTransform2 = glGetUniformLocation(shaderProgramID, "modelMatrix");
	glUniformMatrix4fv(idModelTransform2, 1, GL_FALSE, &M[0][0]);
	glUniform3f(glGetUniformLocation(shaderProgramID, "cameraPos"), camera->getEye().x, camera->getEye().y, camera->getEye().z);



}
