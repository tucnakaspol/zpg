#pragma once

#include "AbstractModel.h"
#include "Models/sphere.h"

class Sphere : public AbstractModel
{

public:
	Sphere();

	void setModel();
	void drawModel();

	//void drawModelWithRotate();
};
