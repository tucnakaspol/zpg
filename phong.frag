#version 400
in vec4 ex_worldPosition;
in vec3 ex_worldNormal;
out vec4 frag_colour;
uniform vec3 cameraPos;

void main(void)
{
    vec3 lightPosition = vec3(0.0f, 10.0f, 0.0f);

    vec3 lightVector = lightPosition - vec3(ex_worldPosition);


    vec3 normal = normalize(ex_worldNormal);    
    vec3 lightDirection = normalize( lightVector);
    float dot_product = max(dot((normal), (lightDirection)), 0.0);

    vec4 diffuse = dot_product * vec4(0.385, 0.647, 0.812, 1.0);       
    vec4 ambient = vec4(0.1, 0.1, 0.1, 1.0);                           

    float specularLight = 0.5f;                                                  
    vec3 viewDirection = normalize(cameraPos - vec3(ex_worldPosition));                             
    vec3 reflectionDirection = reflect((-lightDirection), (normal));                  
    float specAmount = pow(max(dot(viewDirection, (reflectionDirection)), 0.0f), 8);
    float specular = specAmount * specularLight;                                  
    frag_colour = diffuse + ambient + specular;
}