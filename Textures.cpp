#include "Textures.h"

Textures* Textures::instance = 0;

Textures::Textures()
{
	this->textures = new std::vector<GLuint>();
}

Textures* Textures::getInstance()
{
	if (instance == 0)
	{
		instance = new Textures();
	}

	return instance;
}

void Textures::createTexures(const char* name)
{
	GLuint textureID = SOIL_load_OGL_texture(name, SOIL_LOAD_RGBA, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	textures->push_back(textureID);
}



void Textures::setTexture(int textureId)
{
	glActiveTexture(GL_TEXTURE0 + textureId);

	glBindTexture(GL_TEXTURE_2D, textures->at(textureId));
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
}

GLuint Textures::getTextures(int pos)
{
	return this->textures->at(pos);
}
