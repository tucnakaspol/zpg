#pragma once
#include "AbstractModel.h"
#include <vector>

class ModelManager
{
private:
	std::vector<AbstractModel*> models;

public:
	int countOfModels();
	void addModel(AbstractModel*);
	AbstractModel* getModel(int);
};

