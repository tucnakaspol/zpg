#version 400
#define MAX_LIGHTS 4
in vec4 ex_worldPosition;
in vec3 ex_worldNormal;
out vec4 frag_colour;
uniform vec3 cameraPos;
uniform sampler2D textureUnitID;
uniform vec3 lightPositions[MAX_LIGHTS];
uniform vec4 lightColours[MAX_LIGHTS];
uniform float numberOfLights;

in vec2 uv;

void main(void)
{
    vec4 temp = vec4(0);
    vec4 specSum = vec4(0);
    for (int i = 0;i < numberOfLights ;i++)
    {

        vec3 lightVector = lightPositions[i] - vec3(ex_worldPosition);

        float dist = length(lightVector);
        float intensity = 1/(0.005 * dist * dist + 0.001 * dist + 1.0);

        vec3 normal = normalize(ex_worldNormal);    
        vec3 lightDirection = normalize( lightVector);
        float dotProduct = max(dot((normal), (lightDirection)), 0.0);

        vec4 diffuse = dotProduct * lightColours[i];       
        vec4 ambient = (0.1/4) * vec4(1.0, 1.0, 1.0, 1.0);                           

        float specularLight = 0.1f;     
    
        vec3 viewDirection = normalize(cameraPos - vec3(ex_worldPosition));                             
        vec3 reflectionDirection = reflect((-lightDirection), (normal));                  
        float specAmount = pow(max(dot(viewDirection, (reflectionDirection)), 0.0f), 8);
        float specular = specAmount * specularLight;  
        specSum += lightColours[i] * specular;
       
        temp += ambient +  intensity * diffuse; 

    }

   frag_colour = temp * texture(textureUnitID, uv) + specSum;

}