#version 330
layout(location=0) in vec3 position;
layout(location=1) in vec3 colorP;
out vec3 color;
uniform mat4 modelMatrix;
uniform mat4 cameraMatrix;
out vec3 ex_WorldPos;
void main () {
		gl_Position = (cameraMatrix * modelMatrix) * vec4(position, 1.0);
		color = colorP;
		}