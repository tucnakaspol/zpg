#include "Shader.h"
#include "Camera.h"



Shader::Shader(Camera* camera,const char* vertexFile, const char* fragmentFile)
{
	this->camera = camera;

	this->shaderProgramID = ShaderLoader::loadShader(vertexFile, fragmentFile);

}


// Activates the Shader Program
void Shader::ActivateA(glm::mat4 M)
{
	glUseProgram(shaderProgramID);
	GLint idModelTransform = glGetUniformLocation(shaderProgramID, "modelMatrix");
	glUniformMatrix4fv(idModelTransform, 1, GL_FALSE, &M[0][0]);
}

void Shader::ActivateA()
{
	glm::mat4 M = glm::mat4(1.0f);
	glUseProgram(shaderProgramID);
	GLint idModelTransform = glGetUniformLocation(shaderProgramID, "modelMatrix");
	glUniformMatrix4fv(idModelTransform, 1, GL_FALSE, &M[0][0]);
}

void Shader::Activate(glm::mat4 M)
{


	glUseProgram(shaderProgramID);
	GLint idModelTransform1 = glGetUniformLocation(shaderProgramID, "cameraMatrix");
	glUniformMatrix4fv(idModelTransform1, 1, GL_FALSE, glm::value_ptr(camera->getCamera()));
	GLint idModelTransform2 = glGetUniformLocation(shaderProgramID, "modelMatrix");
	glUniformMatrix4fv(idModelTransform2, 1, GL_FALSE, &M[0][0]);
	glUniform3f(glGetUniformLocation(shaderProgramID, "cameraPos"), camera->getEye().x, camera->getEye().y, camera->getEye().z);

}



void Shader::ActivateWithTexture(glm::mat4 M, GLuint idTexture, int position)
{
	glUseProgram(shaderProgramID);


	GLint uniformID = glGetUniformLocation(shaderProgramID, "textureUnitID");
	glUniform1i(uniformID, position);

	GLint idModelTransform1 = glGetUniformLocation(shaderProgramID, "cameraMatrix");
	glUniformMatrix4fv(idModelTransform1, 1, GL_FALSE, glm::value_ptr(camera->getCamera()));
	GLint idModelTransform2 = glGetUniformLocation(shaderProgramID, "modelMatrix");
	glUniformMatrix4fv(idModelTransform2, 1, GL_FALSE, &M[0][0]);
	glUniform3f(glGetUniformLocation(shaderProgramID, "cameraPos"), camera->getEye().x, camera->getEye().y, camera->getEye().z);

}



 
// Deletes the Shader Program
void Shader::Delete()
{
	glDeleteProgram(shaderProgramID);
}

GLuint Shader::getShaderId()
{
	return this->shaderProgramID;
}

Camera* Shader::getCamera()
{
	return this->camera;
}

//glUseProgram(shaderProgram);
//camera->Inputs(WindowInitializer::getInstance()->getWindow());
//camera->Matrix(45.0f, 0.1f, 100.0f, shaderProgram, "camMatrix");
//GLint idModelTransform = glGetUniformLocation(shaderProgram, "modelMatrix");
//glUniformMatrix4fv(idModelTransform, 1, GL_FALSE, &M[0][0]);