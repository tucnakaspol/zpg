#include "Controller.h"

Controller* Controller::instance = 0;

Controller::Controller(GLFWwindow* window, Camera* camera)
{
	this->window = window;
	this->camera = camera;

	glfwSetKeyCallback(window, key_callback);

	glfwSetCursorPosCallback(window, cursor_callback);

	glfwSetMouseButtonCallback(window, button_callback);

	glfwSetWindowFocusCallback(window, window_focus_callback);

	glfwSetWindowIconifyCallback(window, window_iconify_callback);

	glfwSetWindowSizeCallback(window, window_size_callback);

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
}

//Controller* Controller::getInstance()
//{
//	if (instance == 0)
//	{
//		instance = new Controller();
//	}
//	return instance;
//}

void Controller::init(GLFWwindow* window, Camera* camera)
{
	this->window = window;
	this->camera = camera;

	glfwSetKeyCallback(window, key_callback);

	glfwSetCursorPosCallback(window, cursor_callback);

	glfwSetMouseButtonCallback(window, button_callback);

	glfwSetWindowFocusCallback(window, window_focus_callback);

	glfwSetWindowIconifyCallback(window, window_iconify_callback);

	glfwSetWindowSizeCallback(window, window_size_callback);
}
