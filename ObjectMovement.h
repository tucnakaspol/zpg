#pragma once
#include <../glm/vec3.hpp> // glm::vec3
#include <../glm/vec4.hpp> // glm::vec4
#include <../glm/mat4x4.hpp>
class ObjectMovement
{
private:
	glm::mat4 M;
	glm::mat4 A;
	glm::mat4x3 B;
	float t; 
	float delta; 

public:
	ObjectMovement(glm::mat4x3 B);

	glm::vec3 move();
};

