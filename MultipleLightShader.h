#pragma once
#include "Shader.h"
#include "LightManager.h"

class MultipleLightShader : public Shader
{
public:
	MultipleLightShader(Camera* camera, const char* vertexFile, const char* fragmentFile)
		: Shader(camera, vertexFile, fragmentFile)
	{
		
	}

	void ActivateWithTexture(glm::mat4 M, GLuint textureId, int position);
};

