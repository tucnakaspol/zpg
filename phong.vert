#version 400
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;

out vec4 ex_worldPosition;
out vec3 ex_worldNormal;

uniform mat4 cameraMatrix;
uniform mat4 modelMatrix;

void main(void)
{
    gl_Position =  cameraMatrix * modelMatrix * vec4(position, 1.0);
    ex_worldPosition = modelMatrix * vec4(position, 1.0);
    ex_worldNormal= normalize(transpose(inverse(mat3(modelMatrix))) * normal);
}