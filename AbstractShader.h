#pragma once
#include "Camera.h"
class AbstractShader
{
protected:

	Camera* camera = 0;

public:
	virtual void ActivateWithTexture(glm::mat4 M, GLuint textureId, int position) = 0;
	virtual GLuint getShaderId() = 0;
	virtual Camera* getCamera() = 0;
};

