#include "Transformation.h"

Transformation::Transformation()
{
	this->M = glm::mat4(1.0f);
}

glm::mat4 Transformation::scale(float x, float y, float z)
{
	this->M = glm::scale(this->M, glm::vec3(x, y, z));
	return this->M;
}

glm::mat4 Transformation::translate(float x, float y, float z)
{
	this->M = glm::translate(this->M, glm::vec3(x, y, z));
	return this->M;
}

glm::mat4 Transformation::rotateX(float angle)
{
	this->M = glm::rotate(this->M, angle, glm::vec3(1.0f, 0.0f, 0.0f));
	return this->M;
}

glm::mat4 Transformation::rotateY(float angle)
{
	this->M = glm::rotate(M, angle, glm::vec3(0.0f, 1.0f, 0.0f));
	return this->M;
}

glm::mat4 Transformation::rotateZ(float angle)
{
	this->M = glm::rotate(M, angle, glm::vec3(0.0f, 0.0f, 1.0f));
	return this->M;
}

void Transformation::finishTransform(GLuint ID)
{
	GLint idModelTransform = glGetUniformLocation(ID, "modelMatrix");
	glUniformMatrix4fv(idModelTransform, 1, GL_FALSE, &this->M[0][0]);
}

glm::mat4 Transformation::setIdentity()
{
	this->M = glm::mat4(1.0f);
	return this->M;
}

glm::mat4 Transformation::getMatrix()
{
	return this->M;
}


