#pragma once
#include <vector>
#include "DrawableObject.h"
class DrawableObjectManager
{
private:
	DrawableObjectManager();
	std::vector<DrawableObject*> objects;
	static DrawableObjectManager* instance;

public:
	static DrawableObjectManager* getInstance();
	int countOfObjects();
	void addObject(DrawableObject*);
	DrawableObject* getObject(int);
	DrawableObject* getLastObject();
};

