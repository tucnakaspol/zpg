#include "ConstantShader.h"

void ConstantShader::activate(glm::mat4 M)
{
	glUseProgram(shaderProgramID);
	GLint idModelTransform1 = glGetUniformLocation(shaderProgramID, "cameraMatrix");
	glUniformMatrix4fv(idModelTransform1, 1, GL_FALSE, glm::value_ptr(camera->getCamera()));
	GLint idModelTransform2 = glGetUniformLocation(shaderProgramID, "modelMatrix");
	glUniformMatrix4fv(idModelTransform2, 1, GL_FALSE, &M[0][0]);
}
