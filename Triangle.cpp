#include "Triangle.h"


Triangle::Triangle(Shader* shader)
{
	this->shader = shader;
	this->transform = new Transformation();
	this->angle = 1;
	//vertex buffer object (VBO)
	GLuint VBO = 0;
	glGenBuffers(1, &VBO); // generate the VBO
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);

	//Vertex Array Object (VAO)
	glGenVertexArrays(1, &this->VAO); //generate the VAO
	glBindVertexArray(this->VAO); //bind the VAO


	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 8 * sizeof(points[0]), (GLvoid*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 8 * sizeof(points[0]), (GLvoid*)(4 * sizeof(GL_FLOAT)));
	glEnableVertexAttribArray(1);//enable vertex attributes
	//glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
}


void Triangle::setModel()
{
	glBindVertexArray(this->VAO);

}

void Triangle::drawModel()
{	
	this->angle += 0.1;
	this->transform->setIdentity();
	this->transform->translate(0.0f, 0.5f, 0.0f);
	this->transform->rotateX(angle);

	this->shader->Activate(transform->getMatrix());
	this->setModel();
	glDrawArrays(GL_TRIANGLES, 0, 3);
}

