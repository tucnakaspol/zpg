#pragma once
#include "DrawableObjectManager.h"

DrawableObjectManager* DrawableObjectManager::instance = 0;

DrawableObjectManager::DrawableObjectManager()
{

}

DrawableObjectManager* DrawableObjectManager::getInstance()
{
	if (instance == 0)
	{
		instance = new DrawableObjectManager();
	}

	return instance;
}

int DrawableObjectManager::countOfObjects()
{
	return objects.size();
}

void DrawableObjectManager::addObject(DrawableObject* object)
{
	objects.push_back(object);
}

DrawableObject* DrawableObjectManager::getObject(int index)
{
	return objects[index];
}

DrawableObject* DrawableObjectManager::getLastObject()
{
	return objects.back();
}


