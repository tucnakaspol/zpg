#pragma once
#include <vector>
#include "Shader.h"

class ShaderManager
{
private:
	std::vector<Shader*> shaders;
	static ShaderManager* instance;
	ShaderManager();
public:
	int countOfShaders();
	void addShader(Shader*);
	Shader* getShader(int);
	Shader* getShaderWithId(GLuint id);

	static ShaderManager* getInstance();
};

