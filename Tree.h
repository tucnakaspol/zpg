#pragma once

#include "AbstractModel.h"
#include "Shader.h"
#include "Models/tree.h"

class Tree : public AbstractModel
{
public:
	Tree();

	void setModel();
	void drawModel();

	//void drawModelWithRotate();
};
