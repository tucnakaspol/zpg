#pragma once
#include "DrawableObject.h"

class DrawableObjectLight : public DrawableObject
{
	glm::vec3 position;
	glm::vec4 color;
	AbstractShader* shader;

public:
	DrawableObjectLight(AbstractModel* model, AbstractShader* shader, int positionOfTexture, glm::vec4 color)
		: DrawableObject( model,shader, positionOfTexture)
	{
		position = glm::vec3(0.0, 0.0, 0.0);
		this->color = color;
		this->shader = shader;
	}

	void translateObject(float x, float y, float z);

	glm::vec3 getPosition();

	glm::vec4 getColor();

	GLuint getShaderId();

};

