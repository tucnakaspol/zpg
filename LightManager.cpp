#include "LightManager.h"

LightManager* LightManager::instance = 0;


LightManager::LightManager()
{

}



LightManager* LightManager::getInstance()
{
	if (instance == 0)
	{
		instance = new LightManager();
	}

	return instance;
}


int LightManager::countOfLight()
{
	return lights.size();
}

void LightManager::addLight(DrawableObjectLight* model)
{
	lights.push_back(model);
}

DrawableObjectLight* LightManager::getLight(int index)
{
	return lights[index];
}
