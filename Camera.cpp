#include "Camera.h"
#include "Shader.h"
#include "DrawableObjectManager.h"
#include "ShaderManager.h"


Camera* Camera::instance = 0;

Camera::Camera( int width, int height)
{
    this->eye = glm::vec3(0.0f, 0.0f,2.0f);
    this->look = glm::vec3(0.0f, 0.0f, -1.0f);
    this->up = glm::vec3(0.0f, 1.0f, 0.0f);

    this->width = width;
    this->height = height;
	this->projection = glm::mat4(1.0f);
	this->view = glm::mat4(1.0f);

	this->currConst = GLuint(0);
	this->currObject = GLuint(-1);
	this->currShader = GLuint(0);
	this->currMatrix = glm::mat4(1.0f);

}

Camera::Camera()
{

}


Camera* Camera::getInstance(int width, int height)
{
	if (instance == 0)
	{
		instance = new Camera(width, height);
	}

	return instance;
}

Camera* Camera::getInstance()
{
	if (instance == 0)
	{
		instance = new Camera();
	}

	return instance;
}




glm::vec3 Camera::getEye()
{
	return eye;
}

glm::mat4 Camera::getCamera() {
	if (this->height > 0 )
	{
		this->projection = glm::perspective(glm::radians(45.0f), float(this->width / this->height), 0.1f, 200.0f);
		this->view = glm::lookAt(eye, eye + look, up);
	}

    return this->projection * this->view;
}



void Camera::Inputs(GLFWwindow* window)
{

	// Handles key inputs
	
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	{
		eye += speed * -glm::normalize(glm::cross(look, up));
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
	{
		eye += speed * -look;
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
	{
		eye += speed * glm::normalize(glm::cross(look, up));
	}
	
	if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
	{
		eye += speed * -up;
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
	{
		speed = 0.4f;
	}
	else if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_RELEASE)
	{
		speed = 0.1f;
	}

	

	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
	{
		// Hides mouse cursor
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

		// Prevents camera from jumping on the first click
		if (firstLeftClick)
		{
			glfwSetCursorPos(window, (width / 2), (height / 2));
			firstLeftClick = false;
		}

		// Stores the coordinates of the cursor
		double mouseX;
		double mouseY;
		// Fetches the coordinates of the cursor
		glfwGetCursorPos(window, &mouseX, &mouseY);

		// Normalizes and shifts the coordinates of the cursor such that they begin in the middle of the screen
		// and then "transforms" them into degrees 
		float rotX = sensitivity * (float)(mouseY - (height / 2)) / height;
		float rotY = sensitivity * (float)(mouseX - (width / 2)) / width;

		// Calculates upcoming vertical change in the Orientation
		glm::vec3 newOrientation = glm::rotate(look, glm::radians(-rotX), glm::normalize(glm::cross(look, up)));

		// Decides whether or not the next vertical Orientation is legal or not
		if (abs(glm::angle(newOrientation, up) - glm::radians(90.0f)) <= glm::radians(85.0f))
		{
			look = newOrientation;
		}

		// Rotates the Orientation left and right
		look = glm::rotate(look, glm::radians(-rotY), up);

		// Sets mouse cursor to the middle of the screen so that it doesn't end up roaming around
		glfwSetCursorPos(window, (width / 2), (height / 2));
	}else if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_RELEASE)
	{
		// Unhides cursor since camera is not looking around anymore
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		// Makes sure the next time the camera looks around it doesn't jump
		firstLeftClick = true;

	}
	

	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS)
	{
		// Hides mouse cursor
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

		// Prevents camera from jumping on the first click
		if (firstRightClick)
		{
			firstRightClick = false;
			// Stores the coordinates of the cursor
			double mouseX;
			double mouseY;
			// Fetches the coordinates of the cursor
			glfwGetCursorPos(window, &mouseX, &mouseY);

			GLbyte color[4];
			GLfloat depth;
			GLuint index;

			int newy = this->height - mouseY;

			glReadPixels(mouseX, newy, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, color);
			glReadPixels(mouseX, newy, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &depth);
			glReadPixels(mouseX, newy, 1, 1, GL_STENCIL_INDEX, GL_UNSIGNED_INT, &index);

			printf("Clicked on pixel %f, %f, color %02hhx%02hhx%02hhx%02hhx, % f, stencil index % u\n", mouseX, mouseY, color[0], color[1], color[2], color[3], depth, index);

			//M��eme nastavit vybran� t�leso scena->setSelect(index-1);

			//M��eme vypo��st pozici v glob�ln�m sou�adn�m syst�mu.  
			glm::vec3 screenX = glm::vec3(mouseX, newy, depth);
			glm::vec4 viewPort = glm::vec4(0, 0, this->width, this->height);
			glm::vec3 pos = glm::unProject(screenX, this->view, this->projection, viewPort);

			printf("unProject [%f,%f,%f]\n", pos.x, pos.y, pos.z);

			
			currConst = ShaderManager::getInstance()->getShader(0)->getShaderId();

			if (DrawableObjectManager::getInstance()->getObject(index)->getShaderId() != currConst && currObject== GLuint(-1))
			{
				
				currShader = DrawableObjectManager::getInstance()->getObject(index)->getShaderId();
				DrawableObjectManager::getInstance()->getObject(index)->setShader(ShaderManager::getInstance()->getShader(0));
				currObject = index;
				currMatrix = DrawableObjectManager::getInstance()->getObject(index)->getMatrix();
				
			}
			else if(DrawableObjectManager::getInstance()->getObject(index)->getShaderId() == currConst && currObject == index)
			{
				
				DrawableObjectManager::getInstance()->getObject(index)->setShader(ShaderManager::getInstance()->getShaderWithId(currShader));
				currObject = GLuint(-1);
			}
			else
			{
				DrawableObject* obj = new DrawableObject(DrawableObjectManager::getInstance()->getObject(currObject)->getModel(), ShaderManager::getInstance()->getShaderWithId(currShader), DrawableObjectManager::getInstance()->getObject(currObject)->getTexturePos());
				
				glm::vec3 scale;
				glm::quat rotation;
				glm::vec3 translation;
				glm::vec3 skew;
				glm::vec4 perspective;
				glm::decompose(currMatrix, scale, rotation, translation, skew, perspective);
				//obj->scaleObject(scale.x, scale.y, scale.z);
				DrawableObjectManager::getInstance()->addObject(obj);
				printf("%f   %f   %f \n", (float)pos.x, (float)pos.y, (float)pos.z);
				DrawableObjectManager::getInstance()->getLastObject()->translateObject((float)pos.x, (float)pos.y, (float)pos.z);
				DrawableObjectManager::getInstance()->getLastObject()->scaleObject((float)scale.x, (float)scale.y, (float)scale.z);
			}
			


		}



		// Sets mouse cursor to the middle of the screen so that it doesn't end up roaming around-
		glfwSetCursorPos(window, (width / 2), (height / 2));
	}
	else if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_RELEASE)
	{
		// Unhides cursor since camera is not looking around anymore
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		// Makes sure the next time the camera looks around it doesn't jump
		firstRightClick = true;
	}

	
}

void Camera::callSpace() {
		eye += speed * up;
}


void Camera::callW() {
	eye += speed * look;
}


void windowResizeHandler(int windowWidth, int windowHeight) {
	// Use the entire window for rendering.
	glViewport(0, 0, windowWidth, windowHeight);
}

void Camera::change(int width, int height)
{
	this->height = height;
	this->width = width;

}