﻿//Include GLFW  
//#include "GL/GL.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>  

//Include GLM  
#include <../glm/vec3.hpp> // glm::vec3
#include <../glm/vec4.hpp> // glm::vec4
#include <../glm/mat4x4.hpp> // glm::mat4
#include <../glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <../glm/gtc/type_ptr.hpp> // glm::value_ptr
//Include GLEW

//Include the standard C++ headers  `
#include <stdlib.h>
#include <stdio.h>
#include <../../lab01/Application.h>
#include <../../lab01/Shader.h>
#include "Triangle.h"

int main(void)
{
	Application* application = application->getInstance();
	application->draw();

}
