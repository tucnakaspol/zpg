#pragma once
#include "Shader.h"

class ConstantShader : public Shader
{
public:
	ConstantShader(Camera* camera, const char* vertexFile, const char* fragmentFile)
		: Shader(camera, vertexFile, fragmentFile)
	{

	}

	void activate(glm::mat4 M);
};

