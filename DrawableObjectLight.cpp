#include "DrawableObjectLight.h"

void DrawableObjectLight::translateObject(float x, float y, float z)
{
	Transformation* tr = new Transformation();
	this->matrix = this->matrix * tr->translate(x, y, z);

	this->position = glm::vec3(this->position.x + x, this->position.y + y, this->position.z + z);
}

glm::vec3 DrawableObjectLight::getPosition()
{
	return this->position;
}

glm::vec4 DrawableObjectLight::getColor()
{
	return this->color;
}


GLuint DrawableObjectLight::getShaderId()
{
	return this->shader->getShaderId();
}

