#pragma once
#include "Application.h"

Application* Application::instance = 0;


Application::Application()
{
	window;
	glfwSetErrorCallback(error_callback);
	if (!glfwInit()) {
		fprintf(stderr, "ERROR: could not start GLFW3\n");
		exit(EXIT_FAILURE);
	}

	/* //inicializace konkretni verze
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE,
	GLFW_OPENGL_CORE_PROFILE);  //*/

	window = glfwCreateWindow(800, 600, "ZPG", NULL, NULL);
	if (!window) {
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);

	// start GLEW extension handler
	glewExperimental = GL_TRUE;
	glewInit();


	// get version info
	printf("OpenGL Version: %s\n", glGetString(GL_VERSION));
	printf("Using GLEW %s\n", glewGetString(GLEW_VERSION));
	printf("Vendor %s\n", glGetString(GL_VENDOR));
	printf("Renderer %s\n", glGetString(GL_RENDERER));
	printf("GLSL %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	int major, minor, revision;
	glfwGetVersion(&major, &minor, &revision);
	printf("Using GLFW %i.%i.%i\n", major, minor, revision);

	int width, height;
	glfwGetFramebufferSize(window, &width, &height);
	float ratio = width / (float)height;
	glViewport(0, 0, width, height);
}

void Application::error_callback(int error, const char* description)
{
	fputs(description, stderr);
}


Application* Application::getInstance()
{
	if (instance == 0)
	{
		instance = new Application();
	}

	return instance;
}

void Application::draw()
{
	ModelFactory* modelFactory = new ModelFactory();
	ShaderFactory* shaderFactory = new ShaderFactory();
	Textures* textures = Textures::getInstance();

	DrawableObjectManager* drawManager = DrawableObjectManager::getInstance();
	

	int width, height;
	glfwGetWindowSize(window, &width, &height);
	Camera* camera = Camera::getInstance(width, height);

	//Controller::getInstance();
	Controller* controller = new Controller(window, camera);


	shaderFactory->createConstant(camera); // 0
	shaderFactory->createLambert(camera);  //1
	shaderFactory->createPhong(camera);		//2
	shaderFactory->createTextureShader(camera);		//3
	shaderFactory->createMultipleLightShader(camera);	//4

	modelFactory->createPlain(); //0
	modelFactory->createSuziFlat(); //1
	modelFactory->createSuziSmooth(); //2
	modelFactory->createSphere(); //3
	//----------
	modelFactory->createTree(); //4
	modelFactory->createBuilding(); //5 
	modelFactory->createSkydome();  //6
	modelFactory->createZombie();	//7
	modelFactory->createLight(); //8
	modelFactory->createWall(); //9
	

	//----------------
	textures->createTexures("Textures/test.png"); //0
	textures->createTexures("Textures/building.png");  //1
	textures->createTexures("Textures/wooden_fence.png");	//2
	textures->createTexures("Textures/grass.png");		//3
	textures->createTexures("Textures/skydome.png");	//4s
	textures->createTexures("Textures/zombie.png");		//5
	textures->createTexures("Textures/tree.png");	 //6
	textures->createTexures("Textures/bake.png");		//7



	DrawableObject* plain = new DrawableObject(modelFactory->getMM()->getModel(0), ShaderManager::getInstance()->getShader(4), 3);

	Camera* camera = plain->getCamera();

	DrawableObject* build = new DrawableObject(modelFactory->getMM()->getModel(5), ShaderManager::getInstance()->getShader(1), 1);

	DrawableObject* sky = new DrawableObject(modelFactory->getMM()->getModel(6), ShaderManager::getInstance()->getShader(3), 4);
	sky->scaleObject(15, 15, 15);
	DrawableObject* zombie = new DrawableObject(modelFactory->getMM()->getModel(7), ShaderManager::getInstance()->getShader(4), 5);
	zombie->translateObject(-7,0,10);
	DrawableObject* tree = new DrawableObject(modelFactory->getMM()->getModel(4), ShaderManager::getInstance()->getShader(4), 6);
	tree->translateObject(10, 0, 0);
	tree->scaleObject(0.1, 0.1,0.1);

	DrawableObjectLight* light = new DrawableObjectLight(modelFactory->getMM()->getModel(8), ShaderManager::getInstance()->getShader(0), 0, glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
	LightManager::getInstance()->addLight(light);
	light->translateObject(-7, 2, 7);

	DrawableObjectLight* light2 = new DrawableObjectLight(modelFactory->getMM()->getModel(8), ShaderManager::getInstance()->getShader(0), 0, glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));
	LightManager::getInstance()->addLight(light2);
	light2->translateObject(7, 2, 7);

	DrawableObjectLight* light3 = new DrawableObjectLight(modelFactory->getMM()->getModel(8), ShaderManager::getInstance()->getShader(0), 0, glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
	LightManager::getInstance()->addLight(light3);
	light3->translateObject(-7, 2, -7);



	DrawableObjectLight* light4 = new DrawableObjectLight(modelFactory->getMM()->getModel(8), ShaderManager::getInstance()->getShader(0), 0, glm::vec4(1.0f, 1.0f, 1.0f, 1.0f));
	LightManager::getInstance()->addLight(light4);
	light4->translateObject(7, 3, -7);

	DrawableObject* wall = new DrawableObject(modelFactory->getMM()->getModel(9), ShaderManager::getInstance()->getShader(4), 7);
	wall->translateObject(10, 0, 15);
	wall->scaleObject(0.5, 0.5, 0.5);
	wall->rotateObject(120);



	drawManager->addObject(light2);
	drawManager->addObject(plain);
	drawManager->addObject(build);
	drawManager->addObject(sky);
	drawManager->addObject(zombie);
	drawManager->addObject(tree);
	drawManager->addObject(light);
	drawManager->addObject(light3);
	drawManager->addObject(light4);
	drawManager->addObject(wall);
	
	ObjectMovement* objMovement = new ObjectMovement(glm::mat4x3(glm::vec3(-7, 0, -14),
		glm::vec3(20, 0, -20),
		glm::vec3(20, 0, 20),
		glm::vec3(-7, 0, 14)));
	
	
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	while(!glfwWindowShouldClose(window)) {
		
		
		camera->Inputs(window);

		/*plain->drawWithTexture();

	
		build->drawWithTexture();
		sky->drawSkydome();
		zombie->drawWithTranslate(10,0,0);*/

		glm::vec3 p = objMovement->move();
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		drawManager->getObject(4)->translateObjectFromStart(p.x, p.y, p.z);
		for (int i = 0; i < drawManager->countOfObjects(); i++)
		{
			glStencilFunc(GL_ALWAYS, i, 0xFF);
			drawManager->getObject(i)->drawWithTexture();
		}



		// draw triangles
	
		// update other events like input handling
		glfwPollEvents();
		// put the stuff we�ve been drawing onto the display
		glfwSwapBuffers(window);
	}

	glfwDestroyWindow(window);

	glfwTerminate();
	exit(EXIT_SUCCESS);
}
