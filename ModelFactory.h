#pragma once
#include <glm.hpp>
#include "AbstractModel.h"
#include "ModelManager.h"
#include "Triangle.h"
#include "Quad.h"
#include "Plain.h"
#include "SuziFlat.h"
#include "SuziSmooth.h"
#include "Tree.h"
#include "Sphere.h"
#include "Model.h"
#include "Light.h"


class ModelFactory
{
private:
	ModelManager* mm;
public:
	ModelFactory();
	AbstractModel* createQuad(Shader* shader);

	AbstractModel* createTriangle(Shader* shader);

	AbstractModel* createPlain();

	AbstractModel* createBuilding();

	AbstractModel* createSkydome();

	AbstractModel* createZombie();

	AbstractModel* createSuziFlat();

	AbstractModel* createSuziSmooth();

	AbstractModel* createTree();

	AbstractModel* createSphere();

	AbstractModel* createLight();

	AbstractModel* createWall();




	ModelManager* getMM();

};

