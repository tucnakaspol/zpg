#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>  

#include "Camera.h"


class Controller
{
private:
	GLFWwindow* window;
	Camera* camera;
	static Controller* instance;
	
public:

	static int width;
	static int height;

	Controller(GLFWwindow* window, Camera* camera);
	//Controller* getInstance();
	void init(GLFWwindow* window, Camera* camera);

	static void error_callback(int error, const char* description) { fputs(description, stderr); }

	static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
	{
		if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
			glfwSetWindowShouldClose (window, GL_TRUE);
		printf("key_callback [%d,%d,%d,%d] \n", key, scancode, action, mods);
	
	
		if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
		{
			Camera::getInstance()->callSpace();
		}

		int state = glfwGetKey(window, GLFW_KEY_SPACE);
		if (state == GLFW_PRESS)
		{
			Camera::getInstance()->callSpace();
		}

		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		{
			Camera::getInstance()->callW();
		}


	}

	static void window_focus_callback(GLFWwindow* window, int focused) {
		printf("window_focus_callback \n"); 
	}

	static void window_iconify_callback(GLFWwindow* window, int iconified) { printf("window_iconify_callback \n"); }

	static void window_size_callback(GLFWwindow* window, int newWidth, int newHeight) {
		//width = newWidth;
		//height = newHeight;
		printf("resize %d, %d \n", newWidth, newHeight);
		glViewport(0, 0, newWidth, newHeight);

		//instance->changeCam(Controller::width, Controller::height);
		Camera::getInstance()->change(newWidth, newHeight);
		
		
	}

	static void cursor_callback(GLFWwindow* window, double x, double y) { }

	static void button_callback(GLFWwindow* window, int button, int action, int mode) {
		if (action == GLFW_PRESS) printf("button_callback [%d,%d,%d]\n", button, action, mode);
	}

};

