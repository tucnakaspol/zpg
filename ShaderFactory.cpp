#include "ShaderFactory.h"
#include "Shader.h"
#pragma once

ShaderFactory::ShaderFactory()
{
	sm = ShaderManager::getInstance();
}

Shader* ShaderFactory::createConstant(Camera* camera)
{
	ConstantShader* con = new ConstantShader(camera, "constant.vert", "constant.frag");
	this->sm->addShader(con);
	return con;
}


MultipleLightShader* ShaderFactory::createLambert(Camera* camera)
{
	MultipleLightShader* con = new MultipleLightShader(camera, "lambert.vert", "lambert.frag");

	this->sm->addShader(con);

	return con;
}

Shader* ShaderFactory::createPhong(Camera* camera)
{
	Shader* con = new Shader(camera, "phong.vert", "phong.frag");

	this->sm->addShader(con);

	return con;
}

Shader* ShaderFactory::createTextureShader(Camera* camera)
{
	Shader* con = new Shader(camera, "texture.vert", "texture.frag");

	this->sm->addShader(con);

	return con;
}

MultipleLightShader* ShaderFactory::createMultipleLightShader(Camera* camera)
{
	MultipleLightShader* con = new MultipleLightShader(camera, "textureWithlights.vert", "textureWithlights.frag");

	this->sm->addShader(con);

	return con;
}
