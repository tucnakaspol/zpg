#pragma once
#include <vector>
#include "DrawableObjectLight.h"
class LightManager
{
private:
	std::vector<DrawableObjectLight*> lights;
	static LightManager* instance;
	LightManager();

public:
	static LightManager* getInstance();
	int countOfLight();
	void addLight(DrawableObjectLight*);
	DrawableObjectLight* getLight(int);
};

