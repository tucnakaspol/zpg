#pragma once
#include "AbstractModel.h"
#include "Shader.h"
class Quad : public AbstractModel
{
private:
	float points[32] = {
			-0.5f, -0.5f, 0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
			-0.5f, 0.5f, 0.5f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
			0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
			0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f,
	};
		
	Shader* shader;
	Transformation* transform;
	float angle;

public:
	Quad(Shader* shader); 
		
	void setModel();
	void drawModel();
};

