#pragma once
#include <vector>
#include <GL/glew.h>
#include <GLFW/glfw3.h>  
#include <Soil.h>

class Textures
{
    static Textures* instance;

private:
    std::vector<GLuint>* textures;
    Textures();
public:
    static Textures* getInstance();
    void createTexures(const char* name);
    void setTexture(int textureId);
    
    GLuint getTextures(int pos);
};

