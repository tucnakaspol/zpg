#include "ModelManager.h"

int ModelManager::countOfModels()
{
	return models.size();
}

void ModelManager::addModel(AbstractModel* model)
{
	models.push_back(model);
}

AbstractModel* ModelManager::getModel(int index)
{
	return models[index];
}
