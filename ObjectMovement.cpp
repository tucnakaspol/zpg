#include "ObjectMovement.h"


ObjectMovement::ObjectMovement(glm::mat4x3 B)
{
	this->M = glm::mat4(1.0f);
	this->A = glm::mat4(glm::vec4(-1.0, 3.0, -3.0, 1.0),
        glm::vec4(3.0, -6.0, 3.0, 0),
        glm::vec4(-3.0, 3.0, 0, 0),
        glm::vec4(1, 0, 0, 0));
    this->B = B;
    this->t = 0.5f;
    this->delta = 0.002f;
}

glm::vec3 ObjectMovement::move()
{
    glm::vec4 parameters = glm::vec4(t * t * t, t * t, t, 1.0f);
    glm::vec3 p = parameters * A * glm::transpose(B);

    if (t >= 1.0f || t <= 0.0f) 
        this->delta *= -1;

    this->t += this->delta;

    return p;
}
