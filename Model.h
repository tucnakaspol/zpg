#pragma once
#include "AbstractModel.h"
#include<assimp/Importer.hpp>// C++ importerinterface
#include<assimp/scene.h>// aiSceneoutputdata structure
#include<assimp/postprocess.h>// Post processingflags
#include <GL/glew.h>
#include <GLFW/glfw3.h>  

#include <iostream>
#include <../glm/vec3.hpp> // glm::vec3
#include <../glm/vec4.hpp> // glm::vec4
#include <../glm/mat4x4.hpp> // glm::mat4
#include <../glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <../glm/gtc/type_ptr.hpp> // glm::value_ptr
#include<string>
#include<iostream>

class Model : public AbstractModel
{
public:
	int indicesCount;

	struct Vertex
	{
		float Position[3];
		float Normal[3];
		float Texture[2];
		float Tangent[3];
	};

	Model();
	void load(std::string fileName);

	void setModel();
	void drawModel();

};

