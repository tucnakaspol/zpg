#pragma once
#include <GL/glew.h>


class AbstractModel
{
protected:
	GLuint VAO = 0;

public:
	virtual void drawModel() = 0;
	virtual void setModel() = 0;

};

