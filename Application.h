#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>  

#include <iostream>
#include <../glm/vec3.hpp> // glm::vec3
#include <../glm/vec4.hpp> // glm::vec4
#include <../glm/mat4x4.hpp> // glm::mat4
#include <../glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <../glm/gtc/type_ptr.hpp> // glm::value_ptr
//Include GLEW
#pragma once

//Include the standard C++ headers  
#include <stdlib.h>
#include <stdio.h>
#include "ModelFactory.h"
#include "Shader.h"
#include "Camera.h"
#include "Controller.h"
#include "ShaderFactory.h"
#include "ShaderManager.h"
#include "DrawableObject.h"
#include "SOIL.h"
#include "Textures.h"
#include "DrawableObjectManager.h"
#include "DrawableObjectLight.h"
#include "LightManager.h"
#include "ObjectMovement.h"


class Application
{
private:
    /* Here will be the instance stored. */
    static Application* instance;
    GLFWwindow* window;
    /* Private constructor to prevent instancing. */
    Application();

    static void error_callback(int, const char*);



public:
    /* Static access method. */
    static Application* getInstance();

    GLFWwindow* getWindow() {
        return window;
    }

    void draw();
};

