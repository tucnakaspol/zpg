#include "DrawableObject.h"
#include "MultipleLightShader.h"

DrawableObject::DrawableObject(AbstractModel* model, AbstractShader* shader, int positionOfTexture)
{
	this->model = model;
	this->shader = shader;
	this->transform = new Transformation();
	this->angle = 0.0;
	this->positionOfTexture = positionOfTexture;
	this->matrix = glm::mat4(1.0f);
}


void DrawableObject::drawWithTexture()
{
	Textures::getInstance()->setTexture(positionOfTexture);
	this->shader->ActivateWithTexture(this->transform->getMatrix() * matrix, Textures::getInstance()->getTextures(positionOfTexture), positionOfTexture);
	this->model->drawModel();
}

void DrawableObject::drawSkydome()
{
	this->transform->setIdentity();
	this->transform->scale(15, 15,15);
	Textures::getInstance()->setTexture(positionOfTexture);
	this->shader->ActivateWithTexture(this->transform->getMatrix(), Textures::getInstance()->getTextures(positionOfTexture), positionOfTexture);
	this->model->drawModel();
}

void DrawableObject::translateObject(float x, float y, float z)
{
	Transformation* tr = new Transformation();
	this->matrix = this->matrix * tr->translate(x, y, z);
}

void DrawableObject::translateObjectFromStart(float x, float y, float z)
{
	Transformation* tr = new Transformation();
	this->matrix = tr->translate(x, y, z);
}

void DrawableObject::scaleObject(float x,float y, float z)
{
	Transformation* tr = new Transformation();
	this->matrix = this->matrix * tr->scale(x, y, z);
}

void DrawableObject::rotateObject(float y)
{	
	Transformation* tr = new Transformation();
	this->matrix = this->matrix * tr->rotateY(y);
}

void DrawableObject::setShader(Shader* shader)
{
	this->shader = shader;
}

GLuint DrawableObject::getShaderId()
{
	return this->shader->getShaderId();
}

AbstractModel* DrawableObject::getModel()
{
	return this->model;
}

AbstractModel* DrawableObject::getShader()
{
	return this->shader;
}

Camera* DrawableObject::getCamera()
{
	return this->shader->getCamera();
}

int DrawableObject::getTexturePos()
{
	return this->positionOfTexture;
}


glm::mat4 DrawableObject::getMatrix()
{
	return this->matrix;
}

void DrawableObject::setMatrix(glm::mat4 matrix)
{
	this->matrix = matrix;
}

