#pragma once
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <../glm/vec3.hpp> // glm::vec3
#include <../glm/vec4.hpp> // glm::vec4
#include <../glm/mat4x4.hpp> // glm::mat4
#include <../glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <../glm/gtc/type_ptr.hpp> // glm::value_ptr

class Transformation
{
private:
	glm::mat4 M;
public:
	Transformation();
	glm::mat4 scale(float,float,float);
	glm::mat4 translate(float, float,float);

	glm::mat4 rotateX(float);
	glm::mat4 rotateY(float);
	glm::mat4 rotateZ(float);

	void finishTransform(GLuint ID);
	glm::mat4 setIdentity();

	glm::mat4 getMatrix();
};

